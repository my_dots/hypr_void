# My Hypr Void
<br />
  
<div align="center">
<img src="https://gitlab.com/my_dots/hypr_void/-/raw/main/.config/hypr/.img/screen-1716210476.png?ref_type=heads" width="550">
<img src="https://gitlab.com/my_dots/hypr_void/-/raw/main/.config/hypr/.img/screen-1716210465.png?ref_type=heads" width="550">
</div>
<br /><br />

## Установка Void, установка и запуск hyprland, dhcpcd, chrony, pipewire
```
sudo xbps-install chrony dhcpcd

sudo ln -s /etc/sv/dhcpcd /var/service
sudo sv up dhcpcd

sudo ln -s /etc/sv/chronyd /var/service
sudo sv up chronyd
```
```
mkdir .local
mkdir .local/pkgs
cd .local/pkgs

sudo xbps-install git foot noto-fonts-ttf dbus seatd polkit elogind mesa-dri

git clone https://github.com/void-linux/void-packages.git
git clone https://github.com/Makrennel/hyprland-void.git

cd void-packages
./xbps-src binary-bootstrap

cd ..
cd hyprland-void
cat common/shlibs >> ~/.local/pkgs/void-packages/common/shlibs
cp -r srcpkgs/* ~/.local/pkgs/void-packages/srcpkgs
cd
cd ~/.local/pkgs/void-packages

./xbps-src pkg hyprland
./xbps-src pkg xdg-desktop-portal-hyprland
./xbps-src pkg hyprland-protocols

sudo xbps-install -R hostdir/binpkgs hyprland
sudo xbps-install -R hostdir/binpkgs hyprland-protocols
sudo xbps-install -R hostdir/binpkgs xdg-desktop-portal-hyprland

sudo ln -s /etc/sv/dbus /var/service
sudo ln -s /etc/sv/polkitd /var/service
sudo ln -s /etc/sv/seatd /var/service

sudo usermod -aG _seatd $USER

```
```
sudo xbps-install -S pipewire pipewire-devel wireplumber libpulseaudio pulseaudio-utils alsa-pipewire

sudo mkdir -p /etc/alsa/conf.d
sudo ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
sudo ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d
```
## Установка самого необходимого софта

```
sudo xbps-install xorg-server-xwayland firefox pcmanfm grim slurp Waybar wofi nerd-fonts-symbols-ttf pavucontrol mako udiskie curl gsettings-desktop-schemas gvfs jq

cd ~/.local/pkgs/void-packages
./xbps-src pkg hyprpaper
sudo xbps-install -R hostdir/binpkgs hyprpaper
```
Установка менее необходимого софта
```
sudo xbps-install swayimg mpv fish-shell xarchiver gimp micro
```
Скачайте шрифт JetBrainsMono(https://www.jetbrains.com/lp/mono/) и скиньте папку font в /usr/share/fonts 


## Инструкция по подключению не свободных библиотек и установка стима

```
sudo xbps-install -Sy void-repo-nonfree
sudo xbps-install -Sy void-repo-multilib-nonfree
sudo xbps-install void-repo-multilib
sudo xbps-install -Suy

sudo xbps-install steam MangoHud MangoHud-32bit wine winetricks wine-32bit mesa-dri-32bit
libGL-32bit libtxc_dxtn-32bit giflib giflib-32bit libpng libpng-32bit libldap libldap-32bit gnutls gnutls-32bit libopenal libopenal-32bit v4l-utils v4l-utils-32bit libgpg-error libgpg-error-32bit libjpeg-turbo libjpeg-turbo-32bit sqlite sqlite-32bit libXcomposite libXcomposite-32bit libXinerama libXinerama-32bit libgcrypt libgcrypt-32bit ncurses ncurses-libs ncurses-libs-32bit ocl-icd ocl-icd-32bit libxslt libxslt-32bit libva libva-32bit gst-plugins-base1 gst-plugins-base1-32bit amdvlk amdvlk-32bit vulkan-loader vulkan-loader-32bit libX11-devel libX11-devel-32bit libgpg-error libgpg-error-32bit gdk-pixbuf gdk-pixbuf-32bit libgcc libgcc-32bit libglvnd libglvnd-32bit mesa-vulkan-radeon mesa-vulkan-radeon-32bit psmisc fluidsynth libunwind Vulkan-Tools Vulkan-Headers
```
Разумеется, я не знаю для чего нужен каждый из 10000 пакетов, но без них не будет работать steam и portproton, проверял.

Еще установим Portproton со всеми зависимостями, указанными на сайте
```
sudo xbps-install -S bash wget icoutils yad bubblewrap zstd cabextract gzip tar xz openssl desktop-file-utils curl dbus freetype xdg-utils
gdk-pixbuf noto-fonts-ttf nss xrandr lsof mesa-demos ImageMagick Vulkan-Tools libgcc alsa-plugins-32bit libX11-32bit freetype-32bit libglvnd-32bit libgpg-error-32bit nss-32bit openssl-32bit vulkan-loader vulkan-loader-32bit


wget -c "https://github.com/Castro-Fidel/PortProton_ALT/raw/main/portproton" && sh portproton
```
Если последняя команда не исполняется, запустите файлик portproton вручную, мне помогло.

P.s Возможно, легче это всё было установить через flatpak, но я им не пользуюсь

## Чё по багам и использованию
Я человек неприхотливый, поэтому не использую дисплей менеджеры. вход в систему происходит через терминал, а Hyprland стартую командой Hyprland. Юзеры мне советовали использовать ly, чтобы входить в систему как человек, но я этим заниматься не хочу, поэтому вот вам репозиторий, если хотите запариться(github.com/fairyglade/ly).
В конфиге hyprland.conf, который я сюда скинул, этого нет, но я стартую waybar через bash скрипт, который лежит в папке юзера. Он запускает ту же команду waybar'а, что конфиг хайперленда, но с задержкой в 2 секунды(sleep 2). Без этого трей не работет, в следствии чего приложения просто не могут туда свернуться(тот же телеграм из-за такого может отказаться запускаться вообще). Если у вас этого бага нет, то пользуйтесь, если столкнётесь с тем же, либо используйте предложенный костыль, либо чините.
Для прикольного neofetch, я прописал в config.fish 
```
alias neofetch="neofetch --config /home/user/.config/hypr/hypr_void/neofetch/config.conf"
```
В папке с конфигом лежит ещё один, если этот надоест. Ещё там лежит пару картинок на случай, если захочется воспользоваться терминалом kitty(конфиг прилагается) и его возможностью открывать картинки(не забудьте установить пакет ImageMagic)
Также в config.fish можно прописать 
```
alias poweroff="loginctl poweroff"
alias reboot="loginctl reboot"
```
Это чтобы запускать reboot и poweroff без рута. Если верить reddit, помогает не всем.
Перейдите в каталог с файлом weather.sh и дайте ему права на запуск(chmod +x weather.sh).
В конфиге neofetch в графе image-source пропишите абсолютый путь до ascii(в скачанном отсюда конфиге будет прописан относительный путь, но это так не работает).
Иногда может потребоваться запустить Hyprland используя dbus-run-session(корзина в pcmanfm работать без этого не будет), но можно в config.fish прописать:
```
alias Hyprland="dbus-run-session Hyprland"
```


## Ссылки
- [```Прежде всего ссылка на блогера, у которого я брал конфиги и адаптировал их под себя```](https://www.youtube.com/@prolinux2753)
- [```Ссылка на мой mastodon```](https://mastodon.ml/@volandevsrat)